<?php
$productsArray = [
    [
        'title' => 'PHP is awesome',
        'firstName' => 'John',
        'lastName' => 'Doe',
        'price' => 30
    ],
    [
        'title' => 'Javascript is awesome',
        'firstName' => 'Bob',
        'lastName' => 'Robbinson',
        'price' => 25
    ],
    [
        'title' => 'Html is awesome',
        'firstName' => 'Allice',
        'lastName' => 'Grace',
        'price' => 14
    ],
    [
        'title' => 'CSS is awesome',
        'firstName' => 'Ann',
        'lastName' => 'Johnson',
        'price' => 27

    ]
];