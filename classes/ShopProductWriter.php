<?php
class ShopProductWriter{
    public function write(ShopProduct $product){
        return '
            <tr>
                <td>' . $product->title .'</td>
                <td>' . $product->getFullName().'</td>
                <td>' . $product->price .'</td>
                <td>' . $product->getUAHPrice(27.7).'</td>
            </tr>
        ';
    }
}