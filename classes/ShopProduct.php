<?php
class ShopProduct{
    public $title = '';
    public $firstName = '';
    public $lastName = '';
    public $price = 0.0;

    public function __construct($title, $firstName, $lastName, $price){
        $this->title = $title;
        $this->firstName = $firstName;
        $this->lastName = $lastName;
        $this->price = $price;
    }

    public function getFullName(){
        //В созданном объекте обратиться к ..
        return $this->firstName . ' ' . $this->lastName;
    }
    public function getUAHPrice($usdCurrnecy){
        return round($this->price * $usdCurrnecy, 2);
    }
}