<?php
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProduct.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/classes/ShopProductWriter.php';
require_once $_SERVER['DOCUMENT_ROOT'].'/products_data.php';

$productObjects = [];

foreach ($productsArray as $productArr){
    $productObjects[]= new ShopProduct($productArr['title'], 
    $productArr['firstName'], $productArr['lastName'], $productArr['price']);
}

$writer = new ShopProductWriter();
$productObjects[] = new ShopProduct('Learn OOP', 'Bobby', 'Jordan', 17);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">

</head>
<body>
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="col-8">
                <table  class='table'>
                    <tr>
                        <th>Title</th>
                        <th>Author</th>
                        <th>Price</th>
                        <th>Price in UAH</th>
                    </tr>
                    <?php foreach($productObjects as $product):?>
                        <?=$writer->write($product);?>
                    <?php endforeach;?>
                </table>
            </div>
        </div>
    </div>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>